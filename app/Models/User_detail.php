<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_detail extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'user_id',
        'employee_id',
        'address_line1',
        'address_line2',
        'state',
        'city',
        'country',
        'zip_code',
    ];
}
