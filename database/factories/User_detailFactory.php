<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User_detail;

class User_detailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => rand(1,30),
            'employee_id' => rand(111,300),
            'address_line1' => $this->faker->address,
            'address_line2' => $this->faker->address,
            'state' => $this->faker->state,
            'city' => $this->faker->city,
            'country' => $this->faker->country,
            'zip_code' => rand(111,999),
        ];
    }
}
