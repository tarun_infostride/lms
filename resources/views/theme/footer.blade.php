<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2022©</span>
								<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item">
									<a href="https://devs.keenthemes.com" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--begin::Javascript-->
	<script>var hostUrl = "/metronic8/demo1/assets/";</script>
	<!--begin::Global Javascript Bundle(used by all pages)-->
	<script src="{{asset('js/plugins/plugins.bundle.js')}}"></script>
	<script src="{{asset('js/plugins/scripts.bundle.js')}}"></script>
	<!--end::Global Javascript Bundle-->
	<!--begin::Page Vendors Javascript(used by this page)-->
	<script src="{{asset('js/plugins/fullcalendar.bundle.js')}}"></script>
	<script src="{{asset('js/plugins/datatables.bundle.js')}}"></script>
	<!--end::Page Vendors Javascript-->
	<!--begin::Page Custom Javascript(used by this page)-->
	<script src="{{asset('js/plugins/widgets.bundle.js')}}"></script>
	<script src="{{asset('js/plugins/widgets.js')}}"></script>
	<script src="{{asset('js/plugins/chat.js')}}"></script>
	<script src="{{asset('js/plugins/intro.js')}}"></script>
	<script src="{{asset('js/plugins/users-search.js')}}"></script>
	<!--end::Page Custom Javascript-->
	<!--end::Javascript-->